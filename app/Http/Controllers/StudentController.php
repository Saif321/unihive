<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PDO;

class StudentController extends Controller
{
    public function edit_student(Request $request)
    {
        $student = User::where('id', $request->student_id)->first();
        if ($student) {
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $filename = uniqid() . '.' . $image->getClientOriginalExtension();
                // Store the file in the storage path
                $storedFilePath = Storage::disk('public')->putFileAs('images', $image, $filename);
                $student->update([
                    'profile_image' => $storedFilePath,
                ]);
            }
            if ($request->first_name) {
                $student->update([
                    'first_name' => $request->first_name,
                ]);
            }
            if ($request->last_name) {
                $student->update([
                    'last_name' => $request->last_name,
                ]);
            }
            if ($request->college_university) {
                $student->update([
                    'college_university' => $request->college_university,
                ]);
            }
            if ($request->phone_number) {
                $student->update([
                    'phone_number' => $request->phone_number,
                ]);
            }
            return response()->json([
                'status' => "200",
                "message" => "student updated successfully",
            ]);
        } else {
            return response()->json([
                'status' => "404",
                "message" => "student not found",
            ]);
        }
    }

    public function delete(Request $request)
    {
        $student = User::where('id', $request->id)->delete();
        return response()->json([
            'status' => '200',
            'message' => "student deleted successfully",
        ]);
    }

    public function all_students(Request $request)
    {
        $students = User::whereRoleis('student')->get();
        return response()->json([
            'status' => 200,
            'students' => $students,
        ]);
    }

    public function get_student(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required',
        ]);
        $student = User::where('id', $request->id)->first();


        return response()->json([
            'status' => '200',
            'student' => $student,
        ]);
    }
    public function delete_student(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required',
        ]);
        $student = User::where('id', $request->id)->delete();

        if ($student) {
            return response()->json([
                'status' => '200',
                'message' => "student deleted successfully",
            ]);
        } else {
            return response()->json([
                'status' => '200',
                'student' => "no student found",
            ]);
        }
    }
}