<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

// require 'vendor/autoload.php';

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validated = $request->validate([
            'username' => 'required|unique:users|max:255',
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'required',
            'email' => 'required|unique:users'
        ]);


        $storedFilePath = "";
        if ($request->hasFile('image')) {
            $image = $request->file('image');

            // Generate a unique file name
            $filename = uniqid() . '.' . $image->getClientOriginalExtension();

            // Store the file in the storage path
            $storedFilePath = Storage::disk('public')->putFileAs('images', $image, $filename);
        }

        $user = User::create([
            'username' => $request->username,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'password' => Hash::make($request->password),
            'phone_number' => $request->phone_number,
            'college_university' => $request->college_or_university,
            'profile_image' => $storedFilePath,
            'ref_verified_by' => $request->ref_verified_by,
            'email' => $request->email,
        ]);

        if ($user) {
            $token = $user->createToken('Laravel Password Grant Client')->accessToken;
            $student = Role::where('name', 'student')->first();
            $user->attachRole($student);
            return response()->json([
                'status' => '200',
                'message' => 'user register successfully',
                'user' => $user,
                'token' => $token,
            ]);
        }
    }

    public function login(Request $request)
    {
        $validated = $request->validate([
            'password' => 'required',
            'email' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                return response()->json([
                    'status' => '200',
                    'message' => 'user register successfully',
                    'user' => $user,
                    'token' => $token,
                ]);
            } else {
                return response()->json([
                    'status' => '200',
                    'message' => 'invalid password',
                ]);
            }
        } else {
            return response()->json([
                'status' => '200',
                'message' => 'user not found please enter the correct credentials',
            ]);
        }
    }

    public function forgot_password(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required',
        ]);
        $rand = rand(111111, 999999);
        $user = User::where('email', $request->email)->first();

        if ($user) {

            $user->forgot_password_otp = $rand;
            $user->save();

            $email = new \SendGrid\Mail\Mail();
            $email->setFrom("info@uni-hive.net", "Uni Hive");
            $email->setSubject("Forgot your password");
            $email->addTo("saifalich7@gmail.com", "Example User");
            $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
            $email->addContent(
                "text/html",
                "<strong>your forgot password verification code is :</strong>" . $rand
            );
            $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
            try {
                $response = $sendgrid->send($email);
                return response()->json([
                    'status'=>200,
                    'message'=>"mail send successfully"
                ]);
            } catch (Exception $e) {
                echo 'Caught exception: ' . $e->getMessage() . "\n";
            }
        } else {
            return response()->json([
                'status' => '200',
                'message' => 'no account found against provided email',
            ]);
        }
    }
    public function resend_otp(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required',
        ]);
        $user = User::where('email', $request->email)->first();
        if ($user) {
            if ($user->forgot_password_otp) {
                $email = new \SendGrid\Mail\Mail();
                $email->setFrom("info@uni-hive.net", "Uni Hive");
                $email->setSubject("Forgot your password");
                $email->addTo("saifalich7@gmail.com", "Example User");
                $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
                $email->addContent(
                    "text/html",
                    "<strong>your forgot password verification code is :</strong>" . $user->forgot_password_otp
                );
                $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
                try {
                    $response = $sendgrid->send($email);
                    return $response;
                } catch (Exception $e) {
                    echo 'Caught exception: ' . $e->getMessage() . "\n";
                }
            } else {
                $rand = rand(111111, 999999);
                $email = new \SendGrid\Mail\Mail();
                $email->setFrom("info@uni-hive.net", "Uni Hive");
                $email->setSubject("Forgot your password");
                $email->addTo("saifalich7@gmail.com", "Example User");
                $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
                $email->addContent(
                    "text/html",
                    "<strong>your forgot password verification code is :</strong>" . $rand
                );
                $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
                try {
                    $response = $sendgrid->send($email);
                    return $response;
                } catch (Exception $e) {
                    echo 'Caught exception: ' . $e->getMessage() . "\n";
                }
            }
        } else {
            return response()->json([
                'status' => '200',
                'message' => 'invalid email',
            ]);
        }
    }

    public function match_otp(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required',
            'code' => 'required',
        ]);

        $user = User::where('email', $request->email)->first();

        if ($user) {
            if ($user->forgot_password_otp == $request->code) {
                $user->forgot_password_otp = null;
                return response()->json([
                    'status' => '200',
                    'message' => 'otp verified successfully',
                ]);
            } else {
                return response()->json([
                    'status' => '200',
                    'message' => 'invalid otp',
                ]);
            }
            try {
                return response()->json([
                    'status' => '200',
                    'message' => 'no account found against provided email',
                ]);
            } catch (Exception $e) {
                echo 'Caught exception: ' . $e->getMessage() . "\n";
            }
        } else {
            return response()->json([
                'status' => '200',
                'message' => 'no account found against provided email',
            ]);
        }
    }

    public function reset_password(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);

        $user = User::where('email', $request->email)->first();

        if ($user) {
            $user->password = Hash::make($request->password);
            $user->save();
            return response()->json([
                'status' => '200',
                'message' => 'password set successfully',
            ]);
        } else {
            return response()->json([
                'status' => '200',
                'message' => 'no account found against provided email',
            ]);
        }
    }
}
