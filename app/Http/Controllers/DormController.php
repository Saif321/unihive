<?php

namespace App\Http\Controllers;

use App\Models\Dorm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DormController extends Controller
{
    public function add_new_dorm(Request $request)
    {

        // dd($request->images);
        $validated = $request->validate([
            'id' => 'required',
            'description' => 'required',
            'rent_details' => 'required',
        ]);
        $dorm =  Dorm::create([
            'id' => $request->username,
            'description' => $request->description,
            'rent_details' => $request->rent_details,
        ]);
        // return $dorm;
        $storedFilePath = "";
        if ($dorm) {

            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $filename = uniqid() . '.' . $image->getClientOriginalExtension();
                // Store the file in the storage path
                $storedFilePath = Storage::disk('public')->putFileAs('images', $image, $filename);
            }

            $dorm->user_dorm()->attach(auth()->id(), ['image_url' => $storedFilePath]);

            return response()->json([
                'status' => '200',
                'message' => "created successfully"
            ]);
        }
    }

    public function get_all_dorms(Request $request)
    {
        $dorms = Dorm::with('user_dorm')->get();
        return response()->json([
            'status' => '200',
            'dorms' => $dorms,
        ]);
    }

    public function get_specific_dorm(Request $request)
    {
        $validated = $request->validate([
            'dorm_id' => 'required',
        ]);
        $dorm = Dorm::where('id', $request->dorm_id)->with('user_dorm')->first();
        if ($dorm) {
            return response()->json([
                'status' => '200',
                'dorms' => $dorm,
            ]);
        } else {
            return response()->json([
                'status' => '404',
                'message' => "no dorm found",
            ]);
        }
    }
}