<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dorm extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function user_dorm()
    {
        return $this->belongsToMany(User::class)->withPivot('image_url');
    }
}
