<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        // 'name' => 'Test User',
        // 'email' => 'test@example.com',
        // ]);

        $admin = Role::create([
            'name' => 'admin',
            'display_name' => 'User Administrator', // optional
            'description' => 'User is allowed to manage and edit other users and create dorms and crude entities', // optional
        ]);;

        $user = User::create([
            'username' => "admin",
            'first_name' => "admin",
            'last_name' => "admin",
            'password' => Hash::make("password"),
        ]);

        $student = Role::create([
            'name' => 'student',
            'display_name' => 'Student', // optional
            'description' => 'Ability to login with student info', // optional
        ]);

        $user->attachRole($admin->id);
    }
}
