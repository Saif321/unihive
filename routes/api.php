<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BlogPostController;
use App\Http\Controllers\DormController;
use App\Http\Controllers\StudentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/user_register', [AuthController::class, 'register']);
Route::post('/user_login', [AuthController::class, 'login']);
Route::post('/forgot_password', [AuthController::class, 'forgot_password']);
Route::post('/resend_otp', [AuthController::class, 'resend_otp']);
Route::post('/match_otp', [AuthController::class, 'match_otp']);
Route::post('/reset_password', [AuthController::class, 'reset_password']);


Route::group(['middleware' => ['role:admin', 'auth:api']], function () {
    Route::post('/add_new_dorm', [DormController::class, 'add_new_dorm']);
    Route::post('/add_new_blog_post', [BlogPostController::class, 'add_new_blog_post']);
    Route::post('/edit_blog_post', [BlogPostController::class, 'edit_blog_post']);
    Route::post('/delete_blog_post', [BlogPostController::class, 'delete_blog_post']);
    Route::post('/edit_student', [StudentController::class, 'edit_student']);
    Route::post('/delete_student', [StudentController::class, 'delete_student']);
    Route::get('/all_students', [StudentController::class, 'all_students']);
    Route::post('/get_student', [StudentController::class, 'get_student']);
});


Route::group(['middleware' => ['role:student,admin', 'auth:api']], function () {
    Route::get('/get_all_dorms', [DormController::class, 'get_all_dorms']);
    Route::get('/get_all_blogs', [BlogPostController::class, 'get_all_blogs']);
    Route::post('/get_specific_dorm', [DormController::class, 'get_specific_dorm']);
    Route::post('/get_specific_blog', [BlogPostController::class, 'get_specific_blog']);
});
